# React, Redux and Firestone from Scratch

Contains notes and code from studying [React, Redux and Firestone from Scratch](https://www.udemy.com/build-an-app-with-react-redux-and-firestore-from-scratch)

Frameworks covered:

- React
- React Router
- Redux
- Redux Forms
- Revalidate
- Semantic UI
- Google Maps
- Firestone
- React Places Autocomplete
- React Dropzone
- React Cropper
- React FNS
- React Datepicker
- ... and more

Table of content:

<!-- TOC -->

- [React, Redux and Firestone from Scratch](#react-redux-and-firestone-from-scratch)
	- [Section 1: Introduction and getting started](#section-1-introduction-and-getting-started)
	- [Section 2: React Concepts](#section-2-react-concepts)
	- [Section 3: Thinking in React](#section-3-thinking-in-react)
		- [Section 3, Lecture 18: Introduction](#section-3-lecture-18-introduction)
		- [Section 3, Lecture 19: Breaking up UI into components](#section-3-lecture-19-breaking-up-ui-into-components)
		- [Section 3, Lecture 20: Semantic UI: Introduction](#section-3-lecture-20-semantic-ui-introduction)
		- [Section 3, Lecture 25: Passing down static props](#section-3-lecture-25-passing-down-static-props)
		- [Section 3, Lecture 26: Identifying state](#section-3-lecture-26-identifying-state)
		- [Section 3, Lecture 27: Adding application state](#section-3-lecture-27-adding-application-state)
		- [Section 3, Lecture 28: Inverse data flow](#section-3-lecture-28-inverse-data-flow)
		- [Section 3, Lecture 29: Code improvements](#section-3-lecture-29-code-improvements)
	- [Section 4: Forms 101](#section-4-forms-101)
		- [Section 4, Lecture 31: Uncontrolled vs controlled forms](#section-4-lecture-31-uncontrolled-vs-controlled-forms)
		- [Section 4, Lecture 32: Uncontrolled forms](#section-4-lecture-32-uncontrolled-forms)
		- [Section 4, Lecture 33: Controlled forms](#section-4-lecture-33-controlled-forms)
		- [Section 4, Lecture 34: Submitting form data](#section-4-lecture-34-submitting-form-data)
	- [Section 5: Create CRUD apps in React](#section-5-create-crud-apps-in-react)
		- [Section 5, Lecture 37: Selecting event to open](#section-5-lecture-37-selecting-event-to-open)

<!-- /TOC -->

## Section 1: Introduction and getting started

App was created using:

```bash
npx create-react-app revents
```

![create revents app](./doc/img/create_react_app.png)

Recommended **Visual Studio Code extensions**:

- Auto Rename Tag
- Bracket Pair Colorizer
- Debugger for Chrome
- ES7 React/Redux/GraphQL/React-Native snippets
- ESLint
- Material Icon Theme
- npm Intellisense
- Path Intellisense
- Prettier

**Linter** rules:

Create file `.eslintrc` in _revents_ directory:

```json
{
  "extends": "react-app"
}
```

**Debugger**:

Create `launch.json` from Visual Studio Code as Chrome debug launch configuration:

```json
{
  // Use IntelliSense to learn about possible attributes.
  // Hover to view descriptions of existing attributes.
  // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
  "version": "0.2.0",
  "configurations": [
    {
      "type": "chrome",
      "request": "launch",
      "name": "Launch Chrome against localhost",
      "url": "http://localhost:3000",
      "webRoot": "${workspaceFolder}/src"
    }
  ]
}
```

Make sure the `url` port is _3000_ and `webRoot` is `${workspaceFolder}/src`.

Code **Snippets**:

- Install Visual Studio Code Extensions _react redux firestore course snippets_

Snippet shortcuts:

- rcc: create react component
- s_3: start with s_* are the snippets from the course
- imp: import
- imd: import destructive

**Hot module** replacements:

This is not enabled by default in the _create-react-app_. In order to enable we need to make some changes in `index.js`.

Original file:

```js
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
```

Changed file:

```js
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './app/layout/App'
import registerServiceWorker from './registerServiceWorker'

const rootEl = document.getElementById('root')

let render = () => {
  ReactDOM.render(<App/>, rootEl)
}

if (module.hot) {
  module.hot.accept('./app/layout/App', () => {
    setTimeout(render)
  })
}

render()
registerServiceWorker()
```

**Package versions**:

Note: this is deprecated because we are using `yarn`.

We could be using `npm shrinkwrap`. Benefits:

- Dependencies can cause breaking changes
- Dependencies on dependencies can cause breaking changes
- Even minor version updates can cause breaking changes
- Re-purposes package-lock.json into publishable npm-shrinkwrap.json
- It will take precedence over package-lock.json

**Source control**:

The code for the course is hosted at [GitHub repo](https://github.com/TryCatchLearn/revents)

## Section 2: React Concepts

Main functions to use on a regular basis:

![react is easy to learn](./doc/img/react_is_easy_to_learn.png)

Not a framework, does not have opinion on:

- Forms
- Validation
- HTTP API Calls
- State management

Concepts:

- Components (a combination of JavaScript, HTML and CSS)
- Virtual DOM
- One way binding
- JSX

Type support:

- PropTypes (legacy)
- TypeScript
- Flow

State management:

- Plain React: at component level
- Redux: centralized state

## Section 3: Thinking in React

### Section 3, Lecture 18: Introduction

![Thinking in React - steps](./doc/img/thinking_in_react.png)

### Section 3, Lecture 19: Breaking up UI into components

Events list page - application Mock and **breaking into components**

- App
- NavBar
- EventDashboard
- EventList
- EventListItem
- EventListAttendee
- EventActivity

![Mock events page](./doc/img/mock_events_page.png)

![events page files](./doc/img/events_page_components.png)

### Section 3, Lecture 20: Semantic UI: Introduction

Include Semantic UI in the `package.json`:

```json
  "dependencies": {
...
    "semantic-ui-css": "2.2.14",
    "semantic-ui-react": "^0.79.0"
  },

```

Import the Semantic UI CSS in `index.js`:

```js
import 'semantic-ui-css/semantic.min.css'
```

Create Semantic UI elements in a `.jsx` file. HTML vs React way:

```js
import React, { Component } from "react";
import { Button } from 'semantic-ui-react'

class App extends Component {
  render() {
    return (
      <div>
        <h1>Re-vents</h1>

        <button className='ui icon button'>
          <i className='smile icon'></i>
          CSS Button
        </button>

        <Button icon='smile' content='React Button' />
      </div>
    );
  }
}

export default App;
```

### Section 3, Lecture 25: Passing down static props

See branch `section_03-props`:

    _EventDashboard_ passes down _events_ object into the _EventList_ -> _EventListItem_ -> _EventListAttendee_ via props.

EventDashboard.jsx - define mock _events_, send down to _EventList_ as prop:

```js
const eventsDashboard = [
  {
    id: '1',
    title: 'Trip to Tower of London',
    date: '2018-03-27T11:00:00+00:00',
    category: 'culture',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sollicitudin ligula eu leo tincidunt, quis scelerisque magna dapibus. Sed eget ipsum vel arcu vehicula ullamcorper.',
    city: 'London, UK',
    venue: "Tower of London, St Katharine's & Wapping, London",
    hostedBy: 'Bob',
    hostPhotoURL: 'https://randomuser.me/api/portraits/men/20.jpg',
    attendees: [
      {
        id: 'a',
        name: 'Bob',
...
class EventDashboard extends Component {
  render() {
    return (
      <Grid>
        <Grid.Column width={10}>
          <EventList events={eventsDashboard} />
...
```

EventList.jsx - destruct `this.props` into object `events`:

```js
class EventList extends Component {
  render() {
    const {events} = this.props
    return (
      <div>
        <h1>Event List</h1>
        {events.map((event) => (
          <EventListItem key={event.id} event={event}/>
...
```

EventListItem.jsx:

```js
class EventListItem extends Component {
  render() {
    const {event} = this.props
    return (
...
          <List horizontal>
            {event.attendees.map((attendee) => (
              <EventListAttendee key={attendee.id} attendee={attendee}/>
            ))}
          </List>
...
```

EventListAttendee.jsx:

```js
class EventListAttendee extends Component {  
  render() {
    const {attendee} = this.props
    return (
      <List.Item>
        <Image as='a' size='mini' circular src={attendee.photoURL}/>
      </List.Item>
    )
  }
}
```

### Section 3, Lecture 26: Identifying state

We decide based on several different factors:

- Is is passed from the parent via props?

If so: it probably is not a state.

The _events_ are passed down from the _EventDashboard_. So, the events into the _EventList_, _EventListItem_, _EventListAttendee_ are not a state. These are only properties passed via props from the parent.

The _events_ themselves are not being passed into the _EventDashboard_ from parent via props. Thereof they can be considered a state.

- Does it change over time?

If not: probably is not a state.

We have [Create new] button. We are going to change the _events_ list. Thereof they can be considered a state.

- Can you compute it based on other state ot props in your component?

If so: probably not a state.

Identifying state for our app:

![Identifying state for the app](./doc/img/identifying_state_for_the_app.png)

### Section 3, Lecture 27: Adding application state

Determine where state should live:

![where state should live](./doc/img/where_state_should_live.png)

See branch `section_03-state`:

Add state to the _EventsDashboard_:

```js
const eventsDashboard = [
  {
    id: '1',
    title: 'Trip to Tower of London',
    date: '2018-03-27T11:00:00+00:00',
...
class EventDashboard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      events: eventsDashboard,
      isOpen: false
    }
  }
  render() {
    return (
      <Grid>
        <Grid.Column width={10}>
          <EventList events={this.state.events} />
        </Grid.Column>
        <Grid.Column width={6}>
          <Button positive content="Create Event" />
          {this.state.isOpen 
            && <EventForm />}
        </Grid.Column>
      </Grid>
    )
  }
}
...
```

We do the following steps:

- Define constructor
  - Receive `props`
  - Always call `super(props)`
  - Define _initial state_
- Use `this.state` in the component markup instead of referencing the static mock data

### Section 3, Lecture 28: Inverse data flow

We adding `handleFormOpen()` to the EventDashBoard.jsx:

```js
  handleFormOpen() {
    this.setState({
      isOpen: true
    })
  }
```

How do we add call to the method when the button is clicked? If we do just: `<Button onClick={this.handleFormOpen}`, we will get a binding error, the method is not bind to the component class:

![Bind error](./doc/img/this_bind_error.png)

How do we **bind handler method** to the class?

- Use `.bind()` at the end of the constructor

```js
this.handleFormOpen = this.handleFormOpen.bind(this)
```

### Section 3, Lecture 29: Code improvements

They are alternative ways to bind method to component class.

- Use `bind()` inside the `render()` methods:

```js
<Button onClick={this.handleFormOpen.bind(this)}
```

Problem: It is an overhead, will create a new function each time the component renders.

- Use arrow functions:

```js
<Button onClick={() => this.handleFormOpen()}
```

It automatically binds the method to the class.
Problem: It is an overhead, will create a new function each time the component renders.

- Use arrow functions to define handlers:

```js
  handleFormOpen = () => {
    this.setState({
      isOpen: true
    })
  }
```

It does not need to run `bind()` inside the constructor.

How to pass parameter to the function?

```js
  handleFormOpen = (msg) => {
    console.log(msg)
  }
  ...
  <Button onClick={() => this.handleFormOpen('This is a message')}
```

Problem: It is an overhead, will create a new function each time the component renders.

- Use arrow functions of arrow function to define handlers:

```js
  handleFormOpen = (msg) => () => {
    console.log(msg)
  }
  ...
  <Button onClick={this.handleFormOpen('This is a message')}
```

We do not have the overhead of creating a new function each time the component renders.

We do not need to use `bind()` inside the constructor. Since we do not need to do this, we can actually remove the constructor. Instead of:

```js
class EventDashboard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      events: eventsDashboard,
      isOpen: false
    }
  }
```

, we can just have:

```js
class EventDashboard extends Component {
  state = {
    events: eventsDashboard,
    isOpen: false
  }

  handleFormOpen = () => {
    this.setState({
      isOpen: true
    })
  }
  ...
  <Button onClick={this.handleFormOpen}
```

## Section 4: Forms 101

### Section 4, Lecture 31: Uncontrolled vs controlled forms

**Uncontrolled forms** are considered legacy. They skip the virtual DOM. React is not aware of their state. React accesses the value by `ref.name.value`.

**Controlled components** are:

- No direct access to the DOM
- No access to the input
- Only concerned with the state
- Rely on React to manipulate the DOM

We need alter the state and rely on React to manipulate the DOM.

![Controlled forms](./doc/img/controlled_forms.png)

### Section 4, Lecture 32: Uncontrolled forms

IMPORTANT: Uncontrolled forms are legacy. This is just a quick example how to use refs. We will be using controlled components.

```js
class EventForm extends Component {

  onFormSubmit = (evt) => {
    evt.preventDefault()
    console.log(this.refs.title.value)
  }
  render() {
    const {handleCancel} = this.props
    return (
      <Segment>
        <Form onSubmit={this.onFormSubmit}>
          <Form.Field>
            <label>Event Title</label>
            <input ref='title' placeholder="Event Title" />
```

We add `ref` attribute on the element, then we access using `this.refs.title.value`.

They could be used for other things like:

- setting focus

but not for reading actual values.

### Section 4, Lecture 33: Controlled forms

Here is a controlled form example:

```js
class EventForm extends Component {
  state = {
    event: {
      title: '',
      date: '',
      city: '',
      venue: '',
      hostedBy: ''
    }
  }

  onInputChange = (evt) => {
    const newEvent = this.state.event
    newEvent[evt.target.name] = evt.target.value // destructuring assignment
    this.setState({
      event: newEvent
    })
  }

  onFormSubmit = (evt) => {
    evt.preventDefault()
    console.log(this.state.event)
  }

  render() {
    const {handleCancel} = this.props
    const {event} = this.state
    return (
      <Segment>
        <Form onSubmit={this.onFormSubmit}>
          <Form.Field>
            <label>Event Title</label>
            <input
              name='title'
              onChange={this.onInputChange}
              value={event.title}
              placeholder="Event Title"
            />
```

The `onInputChange` is an universal handler. The elements need `title` attributes.

### Section 4, Lecture 34: Submitting form data

## Section 5: Create CRUD apps in React

### Section 5, Lecture 37: Selecting event to open

See `componentDidMount()` and `componentWillReceiveProps()` inside `EventForm.jsx`:

```js
  componentDidMount() {
    if (this.props.selectedEvent !== null) {
      this.setState({
        event: this.props.selectedEvent
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log('current: ', this.props.selectedEvent)
    console.log('next: ', nextProps.selectedEvent)
    if (nextProps.selectedEvent !== this.props.selectedEvent) {
      this.setState({
        event: nextProps.selectedEvent
      })
    }
  }

```